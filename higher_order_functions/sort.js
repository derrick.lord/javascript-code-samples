//Sorting Algorithm Reference: https://codingmiles.com/sorting-algorithms-bubble-sort-using-javascript/

const data = require('./data');
const bubbleSortData = [...data.ages];
const insertionSortData = [...data.ages];
const selectionSortData = [...data.ages];

//Bubble Sort
function bubbleSort(items) {
    var length = items.length;
    for (var i = 0; i < length; i++) { //Number of passes
      for (var j = 0; j < (length - i - 1); j++) { //Notice that j < (length - i)
        //Compare the adjacent positions
        if (items[j] < items[j + 1]) {
          //Swap the numbers
          var tmp = items[j]; //Temporary variable to hold the current number
          items[j] = items[j + 1]; //Replace current number with adjacent number
          items[j + 1] = tmp; //Replace adjacent number with current number
        }
      }
    }
  }


//Selection Sort
function selectionSort(items) {
	var length = items.length;
	for (var i = 0; i < length - 1; i++) {
		//Number of passes
		var min = i; //min holds the current minimum number position for each pass; i holds the Initial min number
		for (var j = i + 1; j < length; j++) { //Note that j = i + 1 as we only need to go through unsorted array
			if (items[j] > items[min]) { //Compare the numbers
				min = j; //Change the current min number position if a smaller num is found
			}
		}
		if (min != i) {
			//After each pass, if the current min num != initial min num, exchange the position.
			//Swap the numbers 
			var tmp = items[i];
			items[i] = items[min];
			items[min] = tmp;
		}
	}
}

//Insertion Sort
function insertionSort(unsortedList) {
	var len = unsortedList.length;
	for (var i = 1; i < len; i++) {
		var tmp = unsortedList[i]; //Copy of the current element. 
		/*Check through the sorted part and compare with the number in tmp. If large, shift the number*/
		for (var j = i - 1; j >= 0 && (unsortedList[j] > tmp); j--) {
			//Shift the number
			unsortedList[j + 1] = unsortedList[j];
		}
		//Insert the copied number at the correct position
		//in sorted part. 
		unsortedList[j + 1] = tmp;
	}
}

//Sort Data with Bubble Sort Algorithm
bubbleSort(bubbleSortData);
console.log('Sorted: Bubble Sort Algorithm:');
console.log(bubbleSortData);

//Sort Data with Selection Sort Algorithm
selectionSort(selectionSortData);
console.log('Sorted: Selection Sort Algorithm');
console.log(selectionSortData);

//Sort Data with Insertion Sort Algorithm
insertionSort(insertionSortData);
console.log('Sorted: Insertion Sort Algorithm');
console.log(insertionSortData);

//Sort Data with Array Prototype Sort method
let sortedData = data.ages.sort( function(a, b) {return a-b} ); 
console.log('Sorted: Array prototype method sort');
console.log(sortedData);