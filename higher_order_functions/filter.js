const data = require('./data');

// All people older than 21 using Traditional Loop
let canDrink = [];
for(let i = 0; i<data.ages.length; i++){
    if(data.ages[i] >= 21){
        canDrink.push(data.ages[i]);
    }
}

console.log(`There are a total of ${canDrink.length} that are over 21!`);
console.log(canDrink);

// all people older than 21 using (filter) Array prototype

let legalAge = data.ages.filter((age)=> age>=21);
console.log(`There are a total of ${legalAge.length} that are allowed to have an Adult beverage!`);
console.log(legalAge);