const data = require('./data');

// Arrow functiont to accumulate total
const reducer = (total, num)=>total+num;


// Traditional total using loop
let totalAgesLoop = 0;
for(let i = 0; i<data.ages.length; i++){
    totalAgesLoop += data.ages[i];
}
console.log(`Total combined ages (via loop): ${totalAgesLoop}`);


// Total using Array prototype reducer
let totalAges = data.ages.reduce(reducer);
console.log(`Total combined ages: ${totalAges}`);
