const data = require('./data');

// Traditional For Each Loop

for (let i = 0; i < data.companies.length; i++){
    console.log(data.companies[i].category);
}


// For Each Loop using Array prototype
data.companies.forEach((company, idx, companies )=> console.log(companies[idx].name.toLowerCase()));
