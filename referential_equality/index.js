const {addItem, addItemPure} = require('./lib');

const a = [1,2,3];
const b = [1,2,3];

const result = addItem(a, 6);
console.log(a);

const pureResult = addItemPure(b, 10);
console.log(pureResult);




