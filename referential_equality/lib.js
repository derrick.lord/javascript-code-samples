/*
Referential Equality 
*/

// Function: AddItem - Mutated Array
module.exports.addItem = (array, item) => array.push(item);

// Function: addItemPure - Pure function with new array
module.exports.addItemPure = (array, item) => [...array, item];







