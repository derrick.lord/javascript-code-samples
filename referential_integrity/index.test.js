const {addItem, addItemPure} = require('./lib');
const a = [1,2,3];
const b = [1,2,3];



describe('Test referential equality', ()=>{
	test('It should respond with result of 4',  ()=>{
        const result = addItem(a, 6);
		    expect(result).toBe(4);
  });

  test('Array should mutated to include the number 8', () => {
      const result = addItem(a, 8);
      expect(a).toContain(8);
      expect(new Set(a)).toContain(8);
  });

  test('Array should not be mutated to include the number 10', () => {
      const result = addItemPure(a, 10);
      expect(result).toContain(10);
      expect(new Set(result)).toContain(10);
  });
});